class Overlay {
  constructor(thumbnailsItems) {
    this.thumbnailsItems = thumbnailsItems;
    this.htmlElement = document.querySelector('html');
    this.overlay = document.querySelector('.overlay');
    this.overlayBtn = this.overlay.querySelector('.overlay__close');
    this.overlayImage = this.overlay.querySelector('.overlay__inner img');
    this.overlayText = this.overlay.querySelector('.overlay__inner p');

    this.init();
  }

  init() {
    this.thumbnailsItems.forEach((el) =>
      el.addEventListener('click', (e) => {
        e.preventDefault();
        this.open(e);
      }),
    );
    this.overlayBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.close();
    });
  }

  open(e) {
    this.overlay.classList.add('open');
    this.overlay.setAttribute('aria-hidden', 'false');
    this.htmlElement.classList.add('o-hidden');
    this.overlayImage.src = e.currentTarget.querySelector(
      'img',
    ).dataset.imageSrc;
    this.overlayImage.alt = e.currentTarget.querySelector('img').alt;
    this.overlayText.innerHTML = e.currentTarget.querySelector('h3').innerHTML;
    setTimeout(() => {
      this.overlayImage.classList.add('loaded');
      this.overlayText.classList.add('loaded');
    }, 300);
  }

  close() {
    this.overlay.classList.remove('open');
    this.overlayImage.classList.remove('loaded');
    this.overlayText.classList.remove('loaded');
    this.overlay.setAttribute('aria-hidden', 'true');
    this.htmlElement.classList.remove('o-hidden');
  }
}

export default Overlay;
