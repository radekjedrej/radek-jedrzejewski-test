/*
  Project: Radek Jedrzejewski Fe Wp Test
  Author: Radek
 */

// Our modules / classes
import Overlay from './modules/overlay';

// Our variables
const thumbnailsItems = document.querySelectorAll('.js-thumbnails_item');

// Assign a new object using our modules/classes
/* eslint-disable no-unused-vars */
if (thumbnailsItems) {
  const overlay = new Overlay(thumbnailsItems);
}
