/* eslint-disable no-param-reassign */

const creatorData = {
  chiselVersion: '1.0.0-alpha.11',
  app: {
    name: 'Radek Jedrzejewski Fe Wp Test',
    author: 'Radek',
    projectType: 'wp-with-fe',
    browsers: ['modern', 'edge18'],
    nameSlug: 'radek-jedrzejewski-fe-wp-test',
    hasJQuery: false,
  },
  wp: {
    title: 'Radek Jedrzejewski Fe Wp Test',
    url: 'http://radek-jedrzejewski-fe-wp-test.test/',
    adminUser: 'radek4686',
    adminEmail: 'radekjedrej@gmail.com',
    tablePrefix: '86yccq3s_',
  },
  wpPlugins: { plugins: ['WP Premium: Advanced Custom Fields Pro'] },
};

const wp = {
  directoryName: 'wp',
  themeName: 'radek-jedrzejewski-fe-wp-test-chisel',
  url: 'http://radek-jedrzejewski-fe-wp-test.test',
};

module.exports = {
  creatorData,

  wp,

  output: {
    base: `${wp.directoryName}/wp-content/themes/${wp.themeName}/dist`,
  },

  // To use React and hot reload for React components:
  // 1. Run `yarn add react-hot-loader @hot-loader/react-dom`
  // 3. Mark your root component as hot-exported as described on
  //    https://github.com/gaearon/react-hot-loader#getting-started (step 2)
  // 4. Uncomment line below
  // react: true,

  plugins: ['chisel-plugin-code-style', 'chisel-plugin-wordpress'],
};
